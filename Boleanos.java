/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package boleanos;

/**
 *
 * @author German
 */
public class Boleanos {

    public String not(String binario) {
        String negacion = "";
        for (int i = 0; i < binario.length(); i++) {
            if ("0".equals(binario.substring(i, i + 1))) {
                negacion += "1";
            } else {
                negacion += "0";
            }
        }
      
        return negacion;
    }

    public String y(String cad1, String cad2) {
        String cad = "";
        String cadd = "";
        String m = "";
        String mm = "";
        String resultado = "";
        for (int i = cad1.length(); i <= 8; i++) {
            if (cad1.length() + cad.length() == 8) {

                m = cad + cad1;

            } else {
                cad += "0";
            }

        }
        for (int i = cad2.length(); i <= 8; i++) {
            if (cad2.length() + cadd.length() == 8) {
                mm = cadd + cad2;

            } else {
                cadd += "0";
            }

        }
        for (int i = 0; i < 8; i++) {
            int a = Integer.parseInt(m.substring(i, i + 1));
            int b = Integer.parseInt(mm.substring(i, i + 1));
            resultado += a * b;
        }
       
        return resultado;


    }

    public String o(String binario1, String binario2) {
        String cad = "";
        String cadd = "";
        String m = "";
        String mm = "";
        for (int i = binario1.length(); i <= 8; i++) {
            if (binario1.length() + cad.length() == 8) {

                binario1 = cad + binario1;

            } else {
                cad += "0";
            }

        }
        for (int i = binario2.length(); i <= 8; i++) {
            if (binario2.length() + cadd.length() == 8) {
                binario2 = cadd + binario2;

            } else {
                cadd += "0";
            }

        }
        if (binario1 == null || binario2 == null) {
            return "";
        }
        int primer_elemento = binario1.length() - 1;
        int segundo_elemento = binario2.length() - 1;
        StringBuilder sb = new StringBuilder();
        int acarreado = 0;
        while (primer_elemento >= 0 || segundo_elemento >= 0) {
            int sum = acarreado;
            if (primer_elemento >= 0) {
                sum += binario1.charAt(primer_elemento) - '0';
                primer_elemento--;
            }
            if (segundo_elemento >= 0) {
                sum += binario2.charAt(segundo_elemento) - '0';
                segundo_elemento--;
            }
            acarreado = sum >> 1;
            sum = sum & 1;
            sb.append(sum == 0 ? '0' : '1');
        }
        if (acarreado > 0) {

            sb.append('1');
        }

        sb.reverse();
        return String.valueOf(sb);
    }

    
    public String f1(String a,String b)
    {
        
        
    return o(not(a), b) ;
    }
    
    public String f2(String a,String b)
    {
    return y(a,not(b));
    }
    public String f3(String a,String b,String s)
    {
    return o(y(b,s),y(a,not(s)));
    }
    public static void main(String[] Args) {
        Boleanos bolean = new Boleanos();
        // bolean.negacion("1110000");
       // bolean.and("11000101", "101");
        //System.out.println(bolean.Suma_Binaria("10", "100"));
System.out.println(bolean.f1("1001","10"));
System.out.println(bolean.f2("1001","10"));
System.out.println(bolean.f3("1110","1110","1110"));
    }
}
